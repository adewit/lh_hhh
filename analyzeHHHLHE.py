import numpy as np
import matplotlib.pyplot as plt
from lhereader import LHEReader

samples_dict = {}
samples_dict['k3_1_k4_75'] = {}
samples_dict['k3_6_k4_75'] = {}
samples_dict['k3_6_k4_1'] = {}
samples_dict['k3_1_k4_75']["file_path"] = "lhe_BSM_HHH_100k/lhe_k3_1_k4_75/events_12345678.lhe"
samples_dict['k3_1_k4_75']["plot_label"] = "k_3=1, k_4=75"
samples_dict['k3_1_k4_75']["plot_colour" ] ="red"
samples_dict['k3_6_k4_75']["file_path"] = "lhe_BSM_HHH_100k/lhe_k3_6_k4_75/unweighted_events.lhe"
samples_dict['k3_6_k4_75']["plot_label"] = "k_3=6, k_4=75"
samples_dict['k3_6_k4_75']["plot_colour" ] ="blue"
samples_dict['k3_6_k4_1']["file_path"] = "lhe_BSM_HHH_100k/lhe_k3_6_k4_1/events_12345.lhe"
samples_dict['k3_6_k4_1']["plot_label"] = "k_3=6, k_4=1"
samples_dict['k3_6_k4_1']["plot_colour" ] ="green"

for key in samples_dict:
  samples_dict[key]['reader'] = LHEReader(samples_dict[key]['file_path'])
  samples_dict[key]['mhhh_list'] = []

  for iev, event in enumerate(samples_dict[key]['reader']):
    # Find Higgs bosons
    higgses = filter(lambda x: abs(x.pdgid)== 25, event.particles)

    # Calculate m_hhh in the event
    combined_p4 = None
    for p4 in map(lambda x: x.p4(), higgses):
      if combined_p4:
        combined_p4 += p4
      else:
        combined_p4 = p4
    samples_dict[key]['mhhh_list'].append(combined_p4.mass)
 
for key in samples_dict:
  plt.hist(samples_dict[key]['mhhh_list'],range=[300, 1000], density=True, bins=70, histtype='step', fill=False, ec=samples_dict[key]['plot_colour'], label=samples_dict[key]['plot_label'])  

plt.legend(loc="upper right")
plt.ylabel('A.U.')
plt.xlabel('m_{HHH} [GeV]');
plt.show()
